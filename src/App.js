import './App.scss';
import Datenschutz from "./datenschutz";
import {useState} from "react";
import Impressum from "./impressum";

function App() {
  const [toggle, setToggle] = useState(false)
  return (
    <div className="App">
      <div className='buttonBar'>
        <div className={'slider ' + (!toggle ? 'left' : '')} />
        <button disabled={!toggle} onClick={() => {setToggle(false)}}>Datenschutz</button>
        <button disabled={toggle} onClick={() => {setToggle(true)}}>Impressum</button>
      </div>
      {toggle ? <Impressum /> : <Datenschutz />}
    </div>
  );
}

export default App;
