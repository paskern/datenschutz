import React from 'react';

function Impressum(props) {
  return (
      <div>
        <h1>Impressum</h1>
        <h2 id="m46">Diensteanbieter</h2>
        <p>Pascal Kern.</p>
        <p>Heitmannstraße 62.</p>
        <p>22083 Hamburg.</p>
        <p>Deutschland.</p>
        <h2 id="m56">Kontaktmöglichkeiten</h2><p>E-Mail-Adresse: <a
          href="mailto:pkernhh@gmail.com">pkernhh@gmail.com</a>.</p>
        <h2 id="m172">Social Media und andere Onlinepräsenzen</h2><p>Dieses Impressum gilt auch für die folgenden
        Social-Media-Präsenzen und Onlineprofile: </p>
        <p><a href="https://www.instagram.com/hh.pascal/" target="_blank">https://www.instagram.com/hh.pascal/</a>.</p>
        <p className="seal"><a href="https://datenschutz-generator.de/?l=de"
                               title="Rechtstext von Dr. Schwenke - für weitere Informationen bitte anklicken."
                               target="_blank" rel="noopener noreferrer nofollow">Erstellt mit kostenlosem
          Datenschutz-Generator.de von Dr. Thomas Schwenke</a></p>
      </div>
  );
}

export default Impressum;